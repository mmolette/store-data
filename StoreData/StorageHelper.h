//
//  StorageHelper.h
//  StoreData
//
//  Created by Mathieu Molette on 28/09/2015.
//  Copyright © 2015 Mathieu Molette. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "DatabaseManager.h"
#import "People.h"

@interface StorageHelper : NSObject

+ (void) storeDataWithUserDefaults;
+ (void) storeDataWithSQLite;
+ (void) storeDataWithCoreData;
+ (void) storeDataWithPlist;

+ (User*) retrieveDataFromUserDefaults;
+ (User*) retrieveDataFromSQLite;
+ (People*) retrieveDataFromCoreData;
+ (User*) retrieveDataFromPlist;

@end
