//
//  DatabaseManager.m
//  StoreData
//
//  Created by Mathieu Molette on 29/09/2015.
//  Copyright © 2015 Mathieu Molette. All rights reserved.
//

#import "DatabaseManager.h"
#import <sqlite3.h>

@implementation DatabaseManager

-(instancetype) initWithDatabaseFilename:(NSString*)filename {
    self = [super init];
    if (self) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        self.pathDirectory = [paths objectAtIndex:0];
        
        self.filename = filename;
        
        [self copyDatabaseIntoDirectory];
    }
    
    return self;
}

-(void) copyDatabaseIntoDirectory {
    
    NSString *targetPath = [self.pathDirectory stringByAppendingPathComponent:self.filename];
    if (![[NSFileManager defaultManager] fileExistsAtPath:targetPath]) {
        
        NSString *sourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:self.filename];
        NSError *error;
        [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:targetPath error:&error];
        
        if(error != nil) {
            NSLog(@"%@", [error localizedDescription]);
        }
    }
}

-(void) runQuery:(const char *)query isQueryExecutable:(BOOL)queryExecutable {
    
    sqlite3 *db;
    
    // Récupère le chemin du fichier BDD
    NSString *dbPath = [self.pathDirectory stringByAppendingPathComponent:self.filename];
    
    // Initialise le tableau des résultats
    if (self.results != nil) {
        [self.results removeAllObjects];
        self.results = nil;
    }
    self.results = [[NSMutableArray alloc] init];
    
    // Initialise le tableau des noms de colonnes
    if (self.columnNames != nil) {
        [self.columnNames removeAllObjects];
        self.columnNames = nil;
    }
    self.columnNames = [[NSMutableArray alloc] init];
    
    // On ouvre la base
    BOOL databaseOpened = sqlite3_open([dbPath UTF8String], &db);
    if(databaseOpened == SQLITE_OK) {
        
        sqlite3_stmt *compiledStatement;
        
        BOOL prepareStatementResult = sqlite3_prepare_v2(db, query, -1, &compiledStatement, NULL);
        if (prepareStatementResult == SQLITE_OK) {
            
            if (!queryExecutable) {
                
                NSMutableArray *dataRow;
                while (sqlite3_step(compiledStatement) == SQLITE_ROW) {
                    
                    dataRow = [[NSMutableArray alloc] init];
                    
                    int nbColumns = sqlite3_column_count(compiledStatement);
                    for (int i = 0; i < nbColumns; i++) {
                        
                        char *dataAsChars = (char*)sqlite3_column_text(compiledStatement, i);
                        
                        if(dataAsChars != NULL) {
                            
                            [dataRow addObject:[NSString stringWithUTF8String:dataAsChars]];
                        }
                        
                        if(self.columnNames.count != nbColumns) {
                            
                            dataAsChars = (char*)sqlite3_column_name(compiledStatement, i);
                            [self.columnNames addObject:[NSString stringWithUTF8String:dataAsChars]];
                        }
                    }
                    
                    if (dataRow.count > 0) {
                        [self.results addObject:dataRow];
                    }
                }
            }else {
                
                int executeQueryResults = sqlite3_step(compiledStatement);
                if (executeQueryResults == SQLITE_DONE) {
                    
                    self.affectedRows = sqlite3_changes(db);
                    self.lastInsertedRowID = sqlite3_last_insert_rowid(db);
                    
                }else {
                    NSLog(@"Database error: %s", sqlite3_errmsg(db));
                }
            }
            
        }else {
            
            NSLog(@"%s", sqlite3_errmsg(db));
        }
        
        sqlite3_finalize(compiledStatement);
    }
    // Ferme la bdd
    sqlite3_close(db);
}

// Méthode permettant de récupérer des données en base
-(NSArray*) loadData:(NSString*)query {
    
    [self runQuery:[query UTF8String] isQueryExecutable:NO];
    
    return (NSArray*)self.results;
}

// Méthode permettant de réaliser des actions en base
-(void)executeQuery:(NSString*)query {
    
    [self runQuery:[query UTF8String] isQueryExecutable:YES];
}

@end
