//
//  AppDelegate.h
//  StoreData
//
//  Created by Mathieu Molette on 28/09/2015.
//  Copyright © 2015 Mathieu Molette. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

#import "User.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property User *user;

+ (AppDelegate *) getAppDelegate;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

