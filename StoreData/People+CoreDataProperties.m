//
//  People+CoreDataProperties.m
//  StoreData
//
//  Created by Mathieu Molette on 22/10/2015.
//  Copyright © 2015 Mathieu Molette. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "People+CoreDataProperties.h"

@implementation People (CoreDataProperties)

@dynamic name;
@dynamic age;
@dynamic picture;

@end
