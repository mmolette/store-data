//
//  People+CoreDataProperties.h
//  StoreData
//
//  Created by Mathieu Molette on 22/10/2015.
//  Copyright © 2015 Mathieu Molette. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "People.h"

NS_ASSUME_NONNULL_BEGIN

@interface People (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *age;
@property (nullable, nonatomic, retain) NSData *picture;

@end

NS_ASSUME_NONNULL_END
