//
//  DatabaseManager.h
//  StoreData
//
//  Created by Mathieu Molette on 29/09/2015.
//  Copyright © 2015 Mathieu Molette. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DatabaseManager : NSObject

@property (nonatomic, strong) NSString *pathDirectory;
@property (nonatomic, strong) NSString *filename;

@property (nonatomic, strong) NSMutableArray *results;

@property (nonatomic, strong) NSMutableArray *columnNames;
@property (nonatomic) int affectedRows;
@property (nonatomic) long lastInsertedRowID;

-(instancetype) initWithDatabaseFilename:(NSString*)filename;
-(void) copyDatabaseIntoDirectory;

-(void)runQuery:(const char *)query isQueryExecutable:(BOOL)queryExecutable;
-(NSArray*) loadData:(NSString*)query;
-(void)executeQuery:(NSString*)query;

@end
