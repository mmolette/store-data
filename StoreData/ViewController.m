//
//  ViewController.m
//  StoreData
//
//  Created by Mathieu Molette on 28/09/2015.
//  Copyright © 2015 Mathieu Molette. All rights reserved.
//

#import "ViewController.h"
#import "StorageHelper.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    // NSUserDefaults
    [StorageHelper storeDataWithUserDefaults];
    [StorageHelper retrieveDataFromUserDefaults];
    
    // SQLite
    [StorageHelper storeDataWithSQLite];
    [StorageHelper retrieveDataFromSQLite];
    
    // Plist
    [StorageHelper storeDataWithPlist];
    [StorageHelper retrieveDataFromPlist];
    
    // CoreData
    [StorageHelper storeDataWithCoreData];
    [StorageHelper retrieveDataFromCoreData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
