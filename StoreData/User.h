//
//  User.h
//  StoreData
//
//  Created by Mathieu Molette on 28/09/2015.
//  Copyright © 2015 Mathieu Molette. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface User : NSObject

@property (nonatomic, retain) UIImage *picture;
@property (nonatomic, retain) NSString *picturePath;
@property (nonatomic, retain) NSString *name;
@property (nonatomic) int age;
@property (nonatomic, retain) NSArray *skills;

@end
