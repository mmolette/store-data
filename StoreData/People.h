//
//  People.h
//  StoreData
//
//  Created by Mathieu Molette on 22/10/2015.
//  Copyright © 2015 Mathieu Molette. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface People : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "People+CoreDataProperties.h"
