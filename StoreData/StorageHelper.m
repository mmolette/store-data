//
//  StorageHelper.m
//  StoreData
//
//  Created by Mathieu Molette on 28/09/2015.
//  Copyright © 2015 Mathieu Molette. All rights reserved.
//

#import "StorageHelper.h"
#import "AppDelegate.h"
#import "User.h"
#import <MagicalRecord/MagicalRecord.h>
#import "People.h"

@implementation StorageHelper


#pragma mark - Store data

+ (void) storeDataWithUserDefaults {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    User *user = [[AppDelegate getAppDelegate] user];
    
    NSData *image = UIImagePNGRepresentation(user.picture);
    
    [defaults setObject:image forKey:@"Image"];
    [defaults setObject:[user picturePath] forKey:@"ImagePath"];
    [defaults setObject:[user name] forKey:@"Username"];
    [defaults setObject:[NSNumber numberWithInt:[user age]] forKey:@"Age"];
    [defaults setObject:[user skills] forKey:@"Skills"];
    
    [defaults synchronize];
}

+ (void) storeDataWithSQLite {
    
    User *user = [[AppDelegate getAppDelegate] user];
    
    NSString *query = [NSString stringWithFormat:@"INSERT INTO User (name, age, picture) VALUES ('%@', '%d', '%@');", user.name, user.age, user.picturePath];
    
    DatabaseManager *dbManager = [[DatabaseManager alloc] initWithDatabaseFilename:@"supinfo.db"];
    
    [dbManager executeQuery:query];
    
    if (dbManager.affectedRows != 0) {
        NSLog(@"%d rows affected", dbManager.affectedRows);
    }else {
        NSLog(@"Could not execute the query !");
    }
}

+ (void) storeDataWithCoreData {
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    User *user = [[AppDelegate getAppDelegate] user];
    
    People *p = [People MR_createEntity];
    [p setName:user.name];
    [p setAge:[NSNumber numberWithInt:user.age]];
    [p setPicture:UIImagePNGRepresentation(user.picture)];
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfAndWait];
    
}

+ (void) storeDataWithPlist {
    
    User *user = [[AppDelegate getAppDelegate] user];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *pathDirectory = [paths objectAtIndex:0];
    NSString *filename = @"supinfo.plist";
    
    NSString *targetPath = [pathDirectory stringByAppendingPathComponent:filename];
    if (![[NSFileManager defaultManager] fileExistsAtPath:targetPath]) {
        
        NSString *sourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:filename];
        NSError *error;
        [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:targetPath error:&error];
        
        if(error != nil) {
            NSLog(@"%@", [error localizedDescription]);
        }
    }
    
    
    
    NSMutableDictionary *userDict = [[NSMutableDictionary alloc] init];
    [userDict setObject:[user name] forKey:@"name"];
    [userDict setObject:[NSNumber numberWithInt:[user age]] forKey:@"age"];
    [userDict setObject:[user picturePath] forKey:@"picturePath"];
    [userDict setObject:[user skills] forKey:@"skills"];
    
    [userDict writeToFile:targetPath atomically:YES];
    
}

#pragma mark - Retrieve data

+ (User*) retrieveDataFromUserDefaults {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    User *user = [[User alloc] init];
    
    [user setPicture:[[UIImage alloc] initWithData:[defaults objectForKey:@"Image"]]];
    [user setPicturePath:[defaults objectForKey:@"ImagePath"]];
    [user setName:[defaults objectForKey:@"Username" ]];
    [user setAge:(int)[defaults objectForKey:@"Age"]];
    [user setSkills:[defaults objectForKey:@"Skills"]];
    
    return user;
}

+ (User*) retrieveDataFromSQLite {
    
    NSString *query = @"SELECT * FROM User;";
    DatabaseManager *dbManager = [[DatabaseManager alloc] initWithDatabaseFilename:@"supinfo.db"];
    
    NSMutableArray *users = [[NSMutableArray alloc] initWithArray:[dbManager loadData:query]];
    
    User *user = nil;
    if(users != nil && [users count] > 0){
        user = [users objectAtIndex:0];
    }
    return user;
}

+ (People*) retrieveDataFromCoreData {
    
    People *p = [People MR_findFirstByAttribute:@"name" withValue:@"Mathieu Molette" ];
    return p;
}

+ (User*) retrieveDataFromPlist {
    
    User *user = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *pathDirectory = [paths objectAtIndex:0];
    NSString *filename = @"supinfo.plist";
    
    NSString *targetPath = [pathDirectory stringByAppendingPathComponent:filename];
    if ([[NSFileManager defaultManager] fileExistsAtPath:targetPath]) {
        
        user = [[User alloc] init];
        
        NSMutableDictionary *userDict = [[NSMutableDictionary alloc] initWithContentsOfFile:targetPath];
        [user setName:[userDict valueForKey:@"name"]];
        [user setAge:(int)[[userDict valueForKey:@"age"] integerValue]];
        [user setPicturePath:[userDict valueForKey:@"picturePath"]];
        [user setSkills:[userDict valueForKey:@"skills"]];
    }

    return user;
}

@end
